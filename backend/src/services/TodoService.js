const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createTodo = async (title, description, dueDate, userId) => {
  try {
    const todo = await prisma.todo.create({
      data: {
        title,
        description,
        dueDate,
        userId
      }
    });
    return todo;
  } catch (error) {
    throw error;
  }
};

const getTodos = async (userId) => {
  try {
    const todos = await prisma.todo.findMany({
      where: {
        userId
      }
    });
    return todos;
  } catch (error) {
    throw error;
  }
};

const updateTodo = async (id, data) => {
  try {
    const todo = await prisma.todo.update({
      where: { id },
      data
    });
    return todo;
  } catch (error) {
    throw error;
  }
};

const deleteTodo = async (id) => {
  try {
    await prisma.todo.delete({
      where: { id }
    });
    return { message: 'Todo deleted successfully.' };
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createTodo,
  getTodos,
  updateTodo,
  deleteTodo
};