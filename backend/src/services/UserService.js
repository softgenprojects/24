const bcrypt = require('@utils/bcrypt.js');
const { sign } = require('@utils/jsonwebtoken.js');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function registerUser(email, password, name) {
  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await prisma.user.create({
      data: {
        email,
        password: hashedPassword,
        name
      }
    });
    return user;
  } catch (error) {
    throw new Error('User registration failed');
  }
}

async function loginUser(email, password) {
  try {
    const user = await prisma.user.findUnique({
      where: { email }
    });
    if (!user) {
      throw new Error('User not found');
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      throw new Error('Invalid password');
    }
    const token = sign({ userId: user.id }, 'secret', { expiresIn: '1h' });
    return { user, token };
  } catch (error) {
    throw new Error('Login failed');
  }
}

async function getUserProfile(userId) {
  try {
    const user = await prisma.user.findUnique({
      where: { id: userId },
      select: { id: true, email: true, name: true }
    });
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  } catch (error) {
    throw new Error('Fetching user profile failed');
  }
}

module.exports = { registerUser, loginUser, getUserProfile };
