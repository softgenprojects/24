const { createTodo, getTodos, updateTodo, deleteTodo } = require('@services/TodoService');

const createTodoController = async (req, res) => {
  try {
    const { title, description, dueDate, userId } = req.body;
    const todo = await createTodo(title, description, dueDate, userId);
    res.status(201).json(todo);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getTodosController = async (req, res) => {
  try {
    const { userId } = req.params;
    const todos = await getTodos(userId);
    res.status(200).json(todos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateTodoController = async (req, res) => {
  try {
    const { id } = req.params;
    const data = req.body;
    const todo = await updateTodo(id, data);
    res.status(200).json(todo);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteTodoController = async (req, res) => {
  try {
    const { id } = req.params;
    const message = await deleteTodo(id);
    res.status(200).json(message);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createTodoController,
  getTodosController,
  updateTodoController,
  deleteTodoController
};