const { registerUser, loginUser, getUserProfile } = require('@services/UserService.js');

async function registerUserController(req, res) {
  const { email, password, name } = req.body;
  try {
    const user = await registerUser(email, password, name);
    res.status(201).json({ user });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}

async function loginUserController(req, res) {
  const { email, password } = req.body;
  try {
    const { user, token } = await loginUser(email, password);
    res.status(200).json({ user, token });
  } catch (error) {
    res.status(401).json({ message: error.message });
  }
}

async function getUserProfileController(req, res) {
  const { userId } = req.params;
  try {
    const userProfile = await getUserProfile(userId);
    res.status(200).json({ userProfile });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
}

module.exports = { registerUserController, loginUserController, getUserProfileController };
