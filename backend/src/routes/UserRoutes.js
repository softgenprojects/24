const express = require('express');
const { registerUserController, loginUserController, getUserProfileController } = require('@controllers/UserController.js');
const { authenticate } = require('@middleware/AuthMiddleware.js');

const router = express.Router();

router.post('/register', registerUserController);
router.post('/login', loginUserController);
router.get('/profile/:userId', authenticate, getUserProfileController);

module.exports = router;