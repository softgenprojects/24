const express = require('express');
const router = express.Router();
const { createTodoController, getTodosController, updateTodoController, deleteTodoController } = require('@controllers/TodoController');
const verifyToken = require('@middleware/AuthMiddleware');

router.post('/topic', verifyToken, createTodoController);
router.get('/topic/:userId', verifyToken, getTodosController);
router.put('/topic/:id', verifyToken, updateTodoController);
router.delete('/topic/:id', verifyToken, deleteTodoController);

module.exports = router;