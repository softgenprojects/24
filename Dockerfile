FROM node:18

RUN apt-get update && apt-get install -y git
RUN git config --global user.email "info@softgen.ai"
RUN git config --global user.name "Softgen.ai"
RUN npm install -g pm2 nodemon

COPY backend/package*.json ./
RUN npm install
COPY backend/ ./

WORKDIR /usr/src/app/frontend
COPY frontend/package*.json ./
RUN npm install
COPY frontend/ ./

WORKDIR /usr/src/app/backend
RUN npx prisma generate

WORKDIR /usr/src/app
COPY . .

EXPOSE 3080 8080

CMD ["tail", "-f", "/dev/null"]


